package rest;

public class EndPoints {

    public static final String METHODS_GET = "get";
    public static final String METHODS_POST = "post";
    public static final String BASIC_AUTH = "basic-auth/";
    public static final String STREAM = "stream/";

}
