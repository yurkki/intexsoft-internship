package annotations;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.HttpBinTests;

@RunWith(value = Suite.class)
@Suite.SuiteClasses(value = HttpBinTests.class)

public class TestSuiteRunner {
}
