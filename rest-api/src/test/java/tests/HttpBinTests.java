package tests;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import models.User;
import org.junit.Assert;
import org.junit.Test;
import rest.Specifications;

import static io.restassured.RestAssured.given;
import static rest.EndPoints.*;

public class HttpBinTests {

    public static final String URL = "http://www.httpbin.org/";

    @Test
    public void validateResponseParameters() {
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpec(200));
        RestAssured
                .given()
                .when()
                .get(URL + METHODS_GET)
                .then().log().all();
    }

    @Test
    public void validateBodyPostRequest() {
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpec(200));
        RestAssured
                .given()
                .when()
                .get(URL + METHODS_POST)
                .then()
                .log().all();
    }

    @Test
    public void checkAuthorization() {

        Specifications.installSpecification(Specifications.requestSpecAuth(URL), Specifications.responseSpec(200));
        User user = new User("1234", "1234");
        RestAssured
                .given()
                .when()
                .get(URL + BASIC_AUTH + user.getUser() + "/" + user.getPassword())
                .then().log().all();
    }

    @Test
    public void checkNegativeAuthorization() {
        Specifications.installSpecification(Specifications.requestSpecAuth(URL), Specifications.responseSpecNeg(401));
        User user = new User("12345", "12345");
        RestAssured
                .given()
                .when()
                .get(URL + BASIC_AUTH + user.getUser() + "/" + user.getPassword())
                .then().log().all();
    }

    @Test
    public void checkStreamRequest() {
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpec(200));
        int streamCount = 2;
        Response response = given()
                .when()
                .get(URL + STREAM + streamCount)
                .then().log().all()
                .extract().response();
        String[] responseArray = response.asString().split("\n");
        Assert.assertEquals(streamCount, responseArray.length);
    }

    @Test
    public void checkStreamRequest101() {
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpec(200));
        int streamCount = 101;
        Response response = given()
                .when()
                .get(URL + STREAM + streamCount)
                .then().log().all()
                .extract().response();
        String[] responseArray = response.asString().split("\n");
        Assert.assertEquals(streamCount, responseArray.length);
    }

    @Test
    public void checkStreamRequest0() {
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpec(200));
        int streamCount = 0;
        Response response = given()
                .when()
                .get(URL + STREAM + streamCount)
                .then().log().all()
                .extract().response();
        Assert.assertEquals( 0, response.asString().length());
    }

    @Test
    public void checkStreamRequestNegative() {
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpecNeg(404));
        int streamCount = -1;
        RestAssured
                .given()
                .when()
                .get(URL + STREAM + streamCount)
                .then().log().all()
                .extract().response();
    }
}




